import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() cardIcon: string | undefined;
  @Input() cardTitle: string | undefined;
  @Input() cardIconColor: string | undefined;

  constructor() {}

  ngOnInit(): void {
  }
}

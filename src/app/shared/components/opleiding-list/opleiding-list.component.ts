import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'opleiding-list',
  templateUrl: './opleiding-list.component.html',
  styleUrls: ['./opleiding-list.component.scss']
})
export class OpleidingListComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() period: string | undefined;
  @Input() description: string | undefined;
  @Input() image: string | undefined;
  @Input() diploma: boolean = false;
  @Input() showDiploma: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'img-container',
  templateUrl: './img-container.component.html',
  styleUrls: ['./img-container.component.scss']
})
export class ImgContainerComponent implements OnInit {
  @Input() imageSrc: string |  undefined;
  @Input() aspectRatio: string = 'aspect-1-1';
  
  constructor() { }

  ngOnInit(): void {
  }

}

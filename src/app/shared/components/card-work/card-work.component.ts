import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'card-work',
  templateUrl: './card-work.component.html',
  styleUrls: ['./card-work.component.scss']
})
export class CardWorkComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() organisation: string | undefined;
  @Input() role: string | undefined;
  @Input() description: string | undefined;
  @Input() period: string | undefined;
  @Input() image: string | undefined;
  @Input() tags: string[] | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}

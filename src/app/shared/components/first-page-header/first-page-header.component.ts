import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'first-page-header',
  templateUrl: './first-page-header.component.html',
  styleUrls: ['./first-page-header.component.scss']
})
export class FirstPageHeaderComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() jobTitle: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}

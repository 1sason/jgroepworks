import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstPageHeaderComponent } from './first-page-header.component';

describe('FirstPageHeaderComponent', () => {
  let component: FirstPageHeaderComponent;
  let fixture: ComponentFixture<FirstPageHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstPageHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstPageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

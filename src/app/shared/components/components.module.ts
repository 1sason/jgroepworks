import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { MaterialModule } from '../material/material.module';
import { AvatarComponent } from './avatar/avatar.component';
import { FirstPageHeaderComponent } from './first-page-header/first-page-header.component';
import { TranslateModule } from '@ngx-translate/core';
import { OpleidingListComponent } from './opleiding-list/opleiding-list.component';
import { CardIconComponent } from './card-icon/card-icon.component';
import { CardWorkComponent } from './card-work/card-work.component';
import { ImgContainerComponent } from './img-container/img-container.component';
import { ChipsComponent } from './chips/chips.component';

@NgModule({
  declarations: [
    CardComponent,
    AvatarComponent,
    FirstPageHeaderComponent,
    OpleidingListComponent,
    CardIconComponent,
    CardWorkComponent,
    ImgContainerComponent,
    ChipsComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    MaterialModule
  ],
  exports: [
    CardComponent,
    AvatarComponent,
    FirstPageHeaderComponent,
    OpleidingListComponent,
    CardIconComponent,
    CardWorkComponent,
    ImgContainerComponent,
    ChipsComponent
  ]
})
export class ComponentsModule { }

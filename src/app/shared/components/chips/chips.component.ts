import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss'],
})
export class ChipsComponent implements OnInit {
  @Input() randomColors: boolean = false;
  @Input() items!: string[];
  @Input() colors: string | undefined;

  constructor() {}

  ngOnInit(): void {}

  colorGenerator() {
    
    if (!this.randomColors) {
      return {};
    }
    const color = `#${this.randomHexNumber()}`;

    return {
      'background-color': color,
      color: 'white',
    };
  }

  private randomHexNumber() {
    return Math.floor(Math.random()*16777215).toString(16);
  }
}

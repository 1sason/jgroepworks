import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'card-icon',
  templateUrl: './card-icon.component.html',
  styleUrls: ['./card-icon.component.scss']
})
export class CardIconComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() description: string | undefined;
  @Input() period: string | undefined;
  @Input() image: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}

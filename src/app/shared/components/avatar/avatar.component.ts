import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: 'avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
  @Input() size: number = 24;

  constructor() { }

  ngOnInit(): void {
  }

  avatarSize(): object {
    return {
      'width': `${this.size}px`,
      'height': `${this.size}px`
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private readonly translate: TranslateService,
    private route: ActivatedRoute
  ) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('nl');
    translate.addLangs(['en']);

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('nl');
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      this.setTransLanguage(params['lang'] || 'nl');
    });
  }

  setTransLanguage(value: string): void {
    this.translate.use(value);
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from '../shared/components/components.module';
import { MaterialModule } from '../shared/material/material.module';
import { PagesContainerComponent } from './pages-container/pages-container.component';
import { PagesComponent } from './pages/pages.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [PagesComponent, PagesContainerComponent],
  exports: [PagesComponent, PagesContainerComponent],
  imports: [CommonModule, TranslateModule, MaterialModule, ComponentsModule],
})
export class ElementsModule { }

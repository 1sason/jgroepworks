export interface CvModel {
  title?: string;
  job_title?: string;
  organization: string;
  versie: string;
  job_reference: string;
  paginas: CvPaginas;
}

export interface CvPaginas {
  voorpagina: CvVoorPaginas;
  skillspagina: SkillsPagina;
  trainingenpagina: TrainingenPagina;
  werkervaring: Werkervaring;
}

export interface CvVoorPaginas {
  personalia: Personalia;
  highlights: Highlights;
  talen: Talen;
  info: Info;
  pitcher: Pitcher;
  opleidingen: Opleidingen;
  werkgevers: Werkgevers;
  branche_ervaring: BrancheErvaring;
}

interface Card {
  toolbar_icon: string;
  icon_color: string;
  toolbar_title: string;
}

interface Personalia extends Card {
  items: PersonaliaItems[];
}

interface PersonaliaItems {
  mat_icon: string;
  title: string;
}

interface Highlights extends Card {
  items: string[];
}

interface Talen extends Card {
  items: TalenItems[];
}

interface TalenItems {
  mat_icon: string;
  niveau: number;
  taal: string;
}

interface Info extends Card {
  info_tekst: string;
  icon: string;
}

interface Pitcher extends Card {
  pitcher_tekst: string;
}

interface Opleidingen extends Card {
  items: OpleidingenItems[];
}

interface OpleidingenItems {
  title: string;
  omschrijving: string;
  periode: string;
  diploma: boolean;
  image: string;
}

interface Werkgevers extends Card {
  items: WerkgeversItems[];
}

interface WerkgeversItems {
  title: string;
  omschrijving: string;
  periode: string;
  image: string;
}

interface BrancheErvaring extends Card {
  items: BrancheErvaringItems[];
}

interface BrancheErvaringItems {
  title: string;
  branches: string[];
}

export interface SkillsPagina {
    info: Info;
    onderwerpen: Onderwerpen[];
}

interface Info extends Card {
    info: string;
}

interface Onderwerpen {
    toolbar_title: string;
    items: OnderwerpenItems[];
}

interface OnderwerpenItems {
    title: string;
    years_experience: string;
}

export interface TrainingenPagina {
    credentials: Credentials;
    cursussen: Cursussen;
}

interface Credentials extends Card {
    items: TrainingenPaginaItems[]
}

interface Cursussen extends Card {
    items: TrainingenPaginaItems[]
}

interface TrainingenPaginaItems {
    title: string;
    omschrijving: string;
    periode: string;
    image: string;
}

export interface Werkervaring {
    pagina_1: WerkervaringPages;
    pagina_2?: WerkervaringPages;
}

export interface WerkervaringPages extends Card {
    items: WerkervaringPagesItems[];
}

interface WerkervaringPagesItems {
    title: string;
    organisatie: string;
    rol: string;
    omschrijving: string;
    periode: string;
    image: string;
    project_kenmerken: string[];
}

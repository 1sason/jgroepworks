import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  @Input() pageNumber: number = 1;
  @Input() showPageHeader: boolean = false;
  @Input() pageTitle: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}

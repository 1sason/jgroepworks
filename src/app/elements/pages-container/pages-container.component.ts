import { Component, OnInit } from '@angular/core';
import { CvModel, CvVoorPaginas, SkillsPagina, TrainingenPagina, Werkervaring } from '../models/cv.model';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-pages-container',
  templateUrl: './pages-container.component.html',
  styleUrls: ['./pages-container.component.scss']
})
export class PagesContainerComponent implements OnInit {
  cv!: CvModel;
  page1!: CvVoorPaginas;
  page2!: SkillsPagina;
  page3!: TrainingenPagina;
  page4!: Werkervaring;

  constructor(private readonly appService: AppService) { }

  ngOnInit(): void {
    this.appService.getCv().subscribe((value: CvModel) => {
      this.cv = value;
      this.page1 = this.cv.paginas.voorpagina;
      this.page2 = this.cv.paginas.skillspagina;
      this.page3 = this.cv.paginas.trainingenpagina;
      this.page4 = this.cv.paginas.werkervaring;
    });
  }

  formatYearExperienceForProgressBar(value: string): number {
    if (!value) {
      return 0;
    }

    const valueInNumber = Number(value);
    return valueInNumber * 33.3;
  }

  formatYearExperience(value: string): string {
    if (!value) {
      return '';
    }

    const valueInNumber = Number(value);
    if (valueInNumber > 5) {
      return '5+';
    } else {
      return value;
    }
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CvModel } from '../models/cv.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpService: HttpClient) { }

  getCv(): Observable<CvModel> {
    return this.httpService.get<CvModel>('./assets/static/cv_structuur.json');
  }
  
}
